1
00:00:03,240 --> 00:00:05,415
They told us we were young.

2
00:00:05,515 --> 00:00:08,604
Yet we understood the enormity of it.

3
00:00:08,900 --> 00:00:11,054
We understood what was at stake.

4
00:00:11,370 --> 00:00:17,204
Our fear was replaced with something altogether more terrifying.

5
00:00:17,450 --> 00:00:18,754
Hope.

6
00:00:18,890 --> 00:00:20,854
Hope is so much worse.

7
00:00:21,200 --> 00:00:23,804
With hope you have something to

8
00:00:22,300 --> 00:00:23,804
...something to

9
00:00:23,300 --> 00:00:23,804
...something to

10
00:00:24,450 --> 00:00:26,804
Oh, for fuck's sake, what's happened now?

11
00:00:27,300 --> 00:00:28,759
Your TV is such a piece of crap.

12
00:00:28,900 --> 00:00:30,404
No it's not, your camera is the problem.

13
00:00:30,780 --> 00:00:32,474
My camera is top of the range.

14
00:00:32,590 --> 00:00:38,434
It's true, his ma only sends him really expensive shit you know, to make up for the fact she doesn't love him.

15
00:00:38,445 --> 00:00:39,504
Michelle!

16
00:00:39,605 --> 00:00:40,804
Oh, sorry. That came out wrong.

17
00:00:41,250 --> 00:00:43,404
I meant...Nope, can't think of any other way to put that.

18
00:00:43,860 --> 00:00:45,904
That actor there looks so familar.

19
00:00:46,110 --> 00:00:46,904
That's you, Orla.

20
00:00:47,310 --> 00:00:48,904
We're gonna have to reshoot this footage.

21
00:00:49,010 --> 00:00:49,904
No chance!

22
00:00:50,060 --> 00:00:51,504
Oh... c'mon Michelle.

23
00:00:51,950 --> 00:00:55,904
(Inhales) These kids from Germany made a short film about the Berlin wall.

24
00:00:56,220 --> 00:00:57,904
And it won an Oscar!

25
00:00:58,100 --> 00:01:00,204
Yeah, but they probably had, you know, talent!

26
00:01:00,450 --> 00:01:05,204
Look, we need to face the fact that we spent the summer making something that's really quite shite.

27
00:01:05,300 --> 00:01:06,150
It's not shit, Michelle.

28
00:01:06,320 --> 00:01:09,290
Well, the script might need a bit of work.

29
00:01:09,520 --> 00:01:11,050
Do not start on the script again.

30
00:01:11,200 --> 00:01:13,770
The script is a masterpiece.

31
00:01:14,090 --> 00:01:15,210
The script is boring, Erin.

32
00:01:15,250 --> 00:01:16,990
It doesn't make any sense. What's it even about?

33
00:01:17,100 --> 00:01:17,600
Peace.

34
00:01:17,750 --> 00:01:20,500
Uh. I am so fucking sick of peace.

35
00:01:20,550 --> 00:01:23,400
It's all anyone ever bangs on about.

36
00:01:23,490 --> 00:01:28,400
OK. I think we should just cut our losses and make a couple of fake videos for Youth and Friend.

37
00:01:28,500 --> 00:01:32,100
Uuuhh. I can fall down three flights of stairs without even hurting myself.

38
00:01:32,500 --> 00:01:34,700
It's two hundred fifty pound a pop, people.

39
00:01:34,800 --> 00:01:35,200
No!

40
00:01:35,400 --> 00:01:36,200
Can I ask something?

41
00:01:36,270 --> 00:01:36,800
Yeah.

42
00:01:36,990 --> 00:01:40,180
How much longer are we gonna ignore the elephant in the room?

43
00:01:40,350 --> 00:01:40,900
Where?

44
00:01:41,250 --> 00:01:43,600
It's tomorrow girls, tomorrow!

45
00:01:44,090 --> 00:01:45,750
Christ, I feel sick.

46
00:01:45,800 --> 00:01:47,250
Why, you're gonna walk it, Claire.

47
00:01:47,490 --> 00:01:48,800
I know that.

48
00:01:49,000 --> 00:01:51,800
I'm not worried about me, I'm worried about you four!

49
00:01:52,000 --> 00:01:54,200
After you fail your GSCEs and school won't take you back.

50
00:01:54,900 --> 00:01:57,850
I don't wanna have to make new friends from scratch. I'll have enough on my plate with the A levels.

51
00:01:58,100 --> 00:02:00,200
I'm just praying you lot scrape by.

52
00:02:00,370 --> 00:02:02,500
That is very considerate of you Claire.

53
00:02:02,670 --> 00:02:03,500
I know.

54
00:02:03,770 --> 00:02:07,500
Why waste your time, you know you're gonna be mine, you know you're gonna be mine...

55
00:02:08,570 --> 00:02:18,700
Once again the maze was the focal point of a troubled peace process today, as Mo Mulland arrived for a visit that has been variously described as mad or brave. 

56
00:02:18,620 --> 00:02:21,510
If we manage to get people around the table on Monday, I think...

57
00:02:21,810 --> 00:02:25,040
God, she's a ballsy wee thing, isn't she?

58
00:02:26,110 --> 00:02:29,260
I've said it before and I'll say it again, if anyone's going to sort that lot out it'll be a woman.

59
00:02:29,380 --> 00:02:30,100
Where is he?

60
00:02:30,570 --> 00:02:31,500
Did you get the butter?

61
00:02:31,570 --> 00:02:32,700
He's done it again. Seamus?

62
00:02:32,850 --> 00:02:33,600
Keep your voice down, Jerry!

63
00:02:33,687 --> 00:02:34,750
I want him out, Mary!

64
00:02:34,870 --> 00:02:35,800
He'll overhear you!

65
00:02:35,970 --> 00:02:38,500
I don't care! He's a psychopath and he's outstayed his welcome.

66
00:02:38,570 --> 00:02:41,100
Seamus! Seamus!

67 
00:02:41,320 --> 00:02:43,780
Just what the hell is your problem boy?

68
00:02:43,910 --> 00:02:46,500
That. That is my problem, Joe. 

68
00:02:46,750 --> 00:02:48,500
There is now a dead shrew on our doorstep.

69
00:02:48,970 --> 00:02:49,500
And?

70
00:02:49,770 --> 00:02:53,820
And if we add that to the three dead pigeons, the dead mouse and dead fecking frog that's quite a bodycount, Joe.

71
00:02:53,970 --> 00:02:55,250
What's that gotta do with Seamus?

72
00:02:55,350 --> 00:03:00,550
The fact that the corpses started piling up within twenty four hours of you taking in a feral cat is no coincidence, Joe.

73
00:03:00,700 --> 00:03:02,800
It wasn't Seamus. Seamus wouldn't hurt a fly.

74
00:03:02,920 --> 00:03:04,520
I've seen him eat flies!

75
00:03:04,530 --> 00:03:05,700  
Seamus didn't kill anything! 

76
00:03:06,000 --> 00:03:08,700
There's a tabby two streets away. 

77
00:03:09,270 --> 00:03:13,040
Has been dead against him. Had a falling out. I don't know the details.

78
00:03:13,170 --> 00:03:16,700
So Seamus has been framed - by another cat?

79
00:03:16,770 --> 00:03:17,500
Exactly!

80
00:03:17,610 --> 00:03:18,500
Jesus! That's desperate.

81
00:03:18,670 --> 00:03:20,500
You could at least put a bell on the thing.

82
00:03:20,670 --> 00:03:23,520
That's a good idea daddy. That way if Seamus is the culprit -

83
00:03:23,520 --> 00:03:24,500
He isn't!

84
00:03:24,600 --> 00:03:27,500
Then the birds, the mice, the shrews, the frogs they'll hear him coming.

85
00:03:27,700 --> 00:03:31,700
Put a bell on him, I don't know Mary, is that not a breech of his human rights?

86
00:03:31,900 --> 00:03:33,900
No, because he's a cat.

87
00:03:34,100 --> 00:03:36,900
He doesn't want to wear a bell, Mary, he's made that very clear!

88
00:03:37,100 --> 00:03:38,900
Christ Almighty!

89
00:03:39,000 --> 00:03:40,900
We're going to Michelle's to watch a film, can we borrow your video card, dad?

90
00:03:41,100 --> 00:03:42,900
So big day tomorrow?

91
00:03:43,100 --> 00:03:44,900
It's already ????

92
00:03:45,100 --> 00:03:46,700
The wee ones get their GSCE results, Sarah.

93
00:03:46,900 --> 00:03:48,100
Oh, I...

94
00:03:48,500 --> 00:03:50,900
Listen, no more filth. Do you hear me?

95
00:03:51,020 --> 00:03:51,800
I'm sorry?

96
00:03:51,950 --> 00:03:56,500
I had to pay an overdue fine last week and American Gigolo is not suitable viewing for teenage girls.

97
00:03:56,000 --> 00:03:59,100
I didn't rent American Gigolo, daddy. I swear!

98
00:03:59,200 --> 00:04:00,200
Leave the wee ones alone, Jerry. Away you go! 

99
00:04:05,200 --> 00:04:06,900
Interesting.

100
00:04:10,200 --> 00:04:11,950
Jesus, this looks class.

101
00:04:12,200 --> 00:04:15,900
This Scottish drag queen takes on the entire English army.

102
00:04:16,100 --> 00:04:19,750
William Wallace wasn't a drag queen, Michelle.

102
00:04:19,900 --> 00:04:22,900
He's wearing a skirt and has a full face of foundation on him, James.

103
00:04:23,100 --> 00:04:26,700
Fuck's sake. You dirty wee bastard. 

104
00:04:26,900 --> 00:04:30,900
What kind of fucking animal doesn't rewind.

105
00:04:31,100 --> 00:04:34,300
Dennis. What are you doing here?
